/**
* Created by JetBrains PhpStorm.
* User: salva
* Date: 16/01/12
*/

(function ($) {
  $(document).ready(function() {

    var toolboxEnabled = false;
    var toolboxToggle = function() {
      $('#tbox-placeholder').toggle();
      toolboxEnabled = !toolboxEnabled;
    }

    $(Drupal.settings.tbox_popup).appendTo($('body'));
    $('<div id="tbox-switch"><input type="checkbox" />Enable Toolbox</div>').appendTo($('body')).click(toolboxToggle);
    $('#tbox-placeholder').toolbox();

    // Disabled for dev.
    if (false) {
      $('<div id="tbox-keys-info"><p align="center">Pressed Keys Info</p></div>').appendTo($('body'));
      $('body').keyup(function(eventObject) {
        drawKeyInfo(eventObject);
      });
    }

    // Draw in screen info about the char pressed.
    function drawKeyInfo(eventObject) {
      var char = String.fromCharCode(eventObject.keyCode);
      var infoRow = "Char: " + char + " - keyCode: " + eventObject.keyCode + " - charCode: " + char.charCodeAt(0) + "<br>";
      $('#tbox-keys-info').append(infoRow);
    }

    // Collapse - expand events.
    $('#tbox-box .tbox-item .tbox-item-header').live('click', function(event) {
      event.stopPropagation();
      var $container = $(this);
      var $content = $(this).parent().find('.tbox-item-content');
      $content.slideToggle({
        duration: 'fast',
        easing: 'linear',
        complete: function() {
          if ($container.hasClass('collapsed')) {
            $container.removeClass('collapsed');
          }
          else {
            $container.addClass('collapsed');
          }
        }
      });
    });

  });

})(jQuery);
