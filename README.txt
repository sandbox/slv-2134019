Toolbox Module
TODO: Complete README
TODO: Move the plugin mechanism to ctools plugins.

Things to do:

Need a way to declare fixed vs dynamic tools. Probably best for now is on array metadata.

FIXED Tools will be fully loaded with the toolbox.
DYNAMIC Tools will be loaded on request, so only basic data will be send to the browser,
so that the user can summon the widget from the toolbox.


It's a performance hit to invoke the hooks every time an ajax TOOL has to be loaded:
I can see 2 possible options for that:

  1.- When invoked the hooks on a normal page load, for ajax-loaded plugins, put the
      needed variables into the Drupal.settings object, so that the javascript file
      triggers the ajax call by passing the plugin callback  (render_callback) as an
      argument (and any other needed argument?).

  2.- After having invoked the hooks on a normal page load, store on the cache table
      their information, with the objects keyed by (ID, in fact). That way, when the javascript
      file triggers the ajax call, it passes the tool ID (So we need an ID for the tools,
      and not only names), we fetch the tools entry from the db, and access directly
      to the object render function. Additionally, with a custom cache table, we
      would only retrieve the actual toold ID object, instead of all tools data.

function toolbox_tbox_tool() {
  $plugin_meta = array(
   /**
    *   'name',
    *   'plugin_callback',¿?
    *   'group',
    *   'type' => 'dynamic' or 'static', // For ajax-loaded tools. // Not implemented.
    *   'file' => '',¿?
    *   'settings' => (collapsed, expanded)
    *   TODO: Folder for 'default' plugins?
    */
  );
  $plugins = array();
  return $plugins;
}