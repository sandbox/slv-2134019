<?php
/**
 * Created by JetBrains PhpStorm.
 * User: salva
 * Date: 12/05/13
 * Time: 02:25
 * To change this template use File | Settings | File Templates.
 */
class TBoxGroup
{

  public $group_name;
  public $expanded;

  function __construct($group_name, $expanded = FALSE) {
    $this->group_name = $group_name;
    $this->expanded = $expanded;
  }

  // Not used for now in groups.
  function render() {}

  public function getGroupName() {
    return $this->group_name;
  }

  public function setGroupName($group_name) {
    $this->group_name = $group_name;
    return $this;
  }

  public function getExpanded() {
    return $this->expanded;
  }

  public function setExpanded($expanded) {
    $this->expanded = $expanded;
    return $this;
  }


}
