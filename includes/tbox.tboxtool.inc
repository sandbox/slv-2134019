<?php
/**
 * Created by JetBrains PhpStorm.
 * User: salva
 * Date: 12/05/13
 * Time: 02:25
 * To change this template use File | Settings | File Templates.
 */
class TBoxTool
{

  public $name;
  public $render_callback;
  public $group;
  public $expanded;

  function __construct($name, $render_callback, $group = NULL, $expanded = FALSE) {
    $this->name = $name;
    $this->render_callback = $render_callback;
    $this->group = $group;
    $this->expanded = $expanded;
    return $this;
  }

  function render() {
    if (function_exists($this->getRenderCallback()) && $render_function = $this->getRenderCallback()) {
      return $render_function();
    }
  }

  public function getName() {
    return $this->name;
  }

  public function setName($name) {
    $this->name = $name;
    return $this;
  }

  public function getRenderCallback() {
    return $this->render_callback;
  }

  public function setRenderCallback($render_callback) {
    $this->render_callback = $render_callback;
    return $this;
  }

  public function getGroup() {
    return $this->group;
  }

  public function setGroup($group) {
    $this->group = $group;
    return $this;
  }

  public function getExpanded() {
    return $this->expanded;
  }

  public function setExpanded($expanded) {
    $this->expanded = $expanded;
    return $this;
  }


}
