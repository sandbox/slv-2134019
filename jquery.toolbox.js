/**
 * @file
 * jquery.toolbox.js
 * Jquery Plugin to render a region of HTML upon certain keystroke combinations.
 */

(function($) {

  jQuery.fn.toolbox = function() {
    var boxContainer = this;

    var toolboxEnabled = false;
    var toolboxToggle = function(elem) {
      elem.toggle('fast');
      toolboxEnabled = !toolboxEnabled;
    }
    // keyCodes: Command --> 91 . alt --> 18 . f1 --> 112

    var fixedMode = false;
    var tempMode = false;
    $('body').keydown(function(eventObject) {
      switch (eventObject.keyCode) {
        case 91:
          fixedMode = true;
          break;
        case 112:
          if (fixedMode) {
            toolboxToggle(boxContainer);
          }
          else {
            // If the toolbox is enabled (possibly fixed, we don't want to hide it for a sec)
            if (!toolboxEnabled) {
              toolboxToggle(boxContainer);
              tempMode = true;
            }
          }
          break;
      }
    }).keyup(function(eventObject) {
        switch (eventObject.keyCode) {
          case 91:
            fixedMode = false;
            break;
          case 112:
            if (tempMode) {
              toolboxToggle(boxContainer);
              tempMode = false;
            }
            break;
        }
      });

  }

})(jQuery);